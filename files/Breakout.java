package breakout;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

/**
 * A Breakout object has a ball, a paddle and a set of bricks. Initially, 30
 * bricks are created, in their corresponding positions, and included in the
 * set. When a brick is destroyed, the brick is removed from the set. When the
 * set is empty the game terminates.
 */
public class Breakout implements GameModel {
	/**
	 * The boolean variable gameover has value true is the game or over, or false
	 * otherwise. The state of the variable is checked by the view to produce the
	 * right message when it ends using the gameOver() method.
	 */
	private boolean gameover;
	/** The score variable keeps track of number of bricks destroyed. */
	private int score;
	private int stage;  // current level
	
	private Random r; // To generate random numbers;

	/** files with the images of the sprites */
	private String paddleImg = "images/paddle.png";
	private String ballImg = "images/ball.png";
	private String[] brickImg = { "images/brickr.png", "images/bricky.png", "images/brickp.png", "images/brickg.png" };

//	private Paddle paddle;
//	private Ball ball;
//	private Set<Brick> bricks;

	/**
	 * When a Breakout game is created, we create a ball, a paddle and the bricks,
	 * organized in 5 rows and 6 columns, each in its corresponding position. The
	 * score is initialized to 0.
	 */
	public Breakout() {
		gameover = false;
		r=new Random(1); // this is the seed for the random generation changing the 1 will generate differente sequences
		score = 0;
		stage=1;
//		paddle = new Paddle(200, 360, paddleImg); // initially at coordinates (200, 360)
//		ball = new Ball(230, 355, ballImg); // initially at coordinates (230, 355)
//		createBricks();
	}

//	private void createBricks() {
//		bricks = new HashSet<>();
//		int previousBrickWidth = 0;
//		int previousBrickHeight = 0;
//		for (int i = 0; i < 5; i++) {
//			for (int j = 0; j < 6; j++) {
//				Brick newBrick = new Brick(j * previousBrickWidth + 30, i * previousBrickHeight + 50, brickImg[i%4]);
//				previousBrickHeight = newBrick.getHeight();
//				previousBrickWidth = newBrick.getWidth();
//				bricks.add(newBrick);
////				bricks.add(new Brick(j * 43 + 30, i * 24 + 50, brickImg[i%4]));
//			}
//		}
//	}

	@Override
	public int getScore() {
		return score;
	}
	
	@Override
	public int getStage() {
		// TO BE COMPLETED
	}
	@Override
	public boolean gameOver() {
		// TO BE COMPLETED
	}

	@Override
	public void gameOver(boolean b) {
		// TO BE COMPLETED
	}
//
//	@Override
//	public Paddle getPaddle() {
//		// TO BE COMPLETED
//	}
//
//	@Override
//	public Ball getBall() {
//		// TO BE COMPLETED
//	}
//
//	@Override
//	public Set<Brick> getBricks() {
//		// TO BE COMPLETED
//	}

	/**
	 * {@inheritDoc}
	 *
	 * If the ball reaches the top, left or right sides, its direction is changed.
	 * The direction of the ball also changes if it touches the paddle or a brick.
	 * To detect such collisions, we check whether the rectangles of the ball and
	 * the other object intersect. If the ball's rectangle intersects with the
	 * rectangle of one of the standing bricks, the brick is removed from the set of
	 * standing bricks and the score is incremented.
	 */
	public void checkCollision() {
//		if (ball.getRectangle().getMaxY() < 0) {
//			ball.setDirectionY(1);
//		}
//		if (ball.getRectangle().getMaxX() > GamePanel.BALL_RIGHT_LIMIT) {
//			ball.setDirectionX(-1);
//		}
//		if (ball.getRectangle().getMinX() < GamePanel.BALL_LEFT_LIMIT) {
//			ball.setDirectionX(1);
//		}
//		if ((ball.getRectangle()).intersects(paddle.getRectangle())) {
//			ball.setDirectionY(-1);
//		}
//		Iterator<Brick> it = bricks.iterator();
//		while (it.hasNext()) {
//			Brick brick = it.next();
//			if (ball.getRectangle().intersects(brick.getRectangle())) {
//				ball.setDirectionY(-1 * ball.getDirectionY());
//				it.remove();
//				score++;
//			}
//		}
	}


	
	@Override
	public String toString() {
//		return String.format("[%s, %s, %d, %d]", ball, paddle, bricks.size(), score);
		return String.format("[%d]", score);
	}
}

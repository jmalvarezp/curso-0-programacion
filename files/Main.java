import breakout.GameModel;
import breakout.Breakout;
import breakout.GameFrame;
import breakout.GameView;
//import breakout.PaddleController;
//import breakout.PeriodicTask;

public class Main {
	public static void main(String[] args) {
		GameModel model = new Breakout();
		GameView view = new GameFrame("Breakout", model);
//		PaddleController paddleCtrl = new PaddleController(model);
//		view.addKeyListener(paddleCtrl);
//		new PeriodicTask(model, view);
	}
}
package breakout;

/**
 * Paddles are sprites that move along the x axis. They have a direction, dx,
 * which can be either -1 (to the left), 0 (stopped), or 1 (to the right).
 */
public class Paddle extends Sprite {

	/**
	 * dx represents the direction of the paddle, which will be either 0 (stopped),
	 * -1 (moving to the left) or 1 (moving to the right)
	 **/
	int dx;

	/**
	 * A paddle is created in the position passed as argument, and using the image
	 * in the file to visualize it. The paddle is created initially stopped
	 *
	 * @param nx   is the initial value for the x coordinate
	 * @param ny   is the initial value for the y coordinate
	 * @param file is the file that contains the image to represent the
	 *             visualization of the sprite
	 */
	public Paddle(int nx, int ny, String file) {
		super(nx, ny, file);
		dx = 0;
	}

	/**
	 * Moving the paddle means changing its x coordinate in the given direction.
	 * After the paddle is moved, its left and right boundaries are checked. If the
	 * paddle is over a limit, its x is set at such limit.
	 */
	public void move() {
		x += dx;
		if (x <= GamePanel.PADDLE_LEFT_LIMIT) {
			x = GamePanel.PADDLE_LEFT_LIMIT;
		} else if (x >= GamePanel.PADDLE_RIGHT_LIMIT) {
			x = GamePanel.PADDLE_RIGHT_LIMIT;
		}
	}

	/**
	 * Updates the dx variable with the value passed as argument.
	 *
	 * @param ndx int value to be set as direction of the paddle
	 */
	public void setDirection(int ndx) {
		// TO BE COMPLETED
	}
}

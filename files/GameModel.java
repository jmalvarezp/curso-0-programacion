package breakout;

import java.util.Set;

/**
 * Model component (following the MVC design pattern) of the implementation of
 * the Breakout game.
 */
public interface GameModel {

	/**
	 * Returns true if the game is over, false otherwise.
	 * 
	 * @return boolean value indicating whether the game is over or not.
	 */
	boolean gameOver();

	/**
	 * Returns the paddle.
	 * 
	 * @return paddle of the game
	 */
//	Paddle getPaddle();

	/**
	 * Returns the ball.
	 * 
	 * @return ball of the game
	 */
//	Ball getBall();

	/**
	 * Returns the set of bricks.
	 * 
	 * @return set of bricks of the game
	 */
//	Set<Brick> getBricks();

	/**
	 * Returns the current score.
	 * 
	 * @return current score
	 */
	int getScore();
	
	/**
	 * Returns the current stage.
	 * 
	 * @return current stage
	 */
	int getStage();

	/**
	 * Checks whether the ball has reached a side of the playing panel or has
	 * collided with the paddle or a brick.
	 */
	void checkCollision();

	/**
	 * Sets the state of the game as indicated by the argument.
	 * 
	 * @param b boolean value indicating whether the game is over or not.
	 */
	void gameOver(boolean b);
}

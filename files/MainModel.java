import breakout.Breakout;
import breakout.GameModel;
import breakout.GamePanel;

public class MainModel {
	public static void main(String[] args) {
		GameModel model = new Breakout();
		while (!model.gameOver()) {
			System.out.println(model);
			model.getBall().move();
			if (model.getBricks().size() == 0) {
				model.gameOver(true);
				System.out.println("You win!");
			} else if (model.getBall().getRectangle().getMinY() > GamePanel.BOTTOM) {
				model.gameOver(true);
				System.out.println("Game over!");
			} else {
				model.checkCollision();
			}
		}
	}
}

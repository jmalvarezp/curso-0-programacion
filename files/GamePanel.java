package breakout;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Toolkit;

import javax.swing.JPanel;

public class GamePanel extends JPanel {
	private static final long serialVersionUID = 6725062741246558377L;
	public static final int WIDTH = 325;
	public static final int HEIGTH = 400;
	public static final int BOTTOM = HEIGTH - 10;
	public static final int PADDLE_LEFT_LIMIT = 2;
	public static final int PADDLE_RIGHT_LIMIT = WIDTH - 50;
	public static final int BALL_LEFT_LIMIT = 0;
	public static final int BALL_RIGHT_LIMIT = WIDTH - 20;

	private GameModel model;
	private String message;

	public GamePanel(GameModel m) {
		setPreferredSize(new Dimension(WIDTH, HEIGTH));
		model = m;
	}

	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (model.gameOver()) {
			Font font = new Font("Verdana", Font.BOLD, 18);
			FontMetrics metr = this.getFontMetrics(font);
			g.setColor(Color.BLACK);
			g.setFont(font);
			g.drawString(message, (GamePanel.WIDTH - metr.stringWidth(message)) / 2, GamePanel.WIDTH / 2);
		} else {
//			Paddle paddle = model.getPaddle();
//			g.drawImage(paddle.getImage(), paddle.getX(), paddle.getY(), paddle.getWidth(), paddle.getHeight(), this);
//			Ball ball = model.getBall();
//			g.drawImage(ball.getImage(), ball.getX(), ball.getY(), ball.getWidth(), ball.getHeight(), this);
//			for (Brick brick : model.getBricks()) {
//				g.drawImage(brick.getImage(), brick.getX(), brick.getY(), brick.getWidth(), brick.getHeight(), this);
//			}
		}
		Toolkit.getDefaultToolkit().sync(); // this method ensures that the display is up to date
		g.dispose();
	}

	/**
	 * Sets the final message, which can be either "You win!", if all
	 * bricks are broken, or "Game over!", if the ball touches the bottom of the
	 * court.
	 */
	public void setMessage(String msg) {
		message = msg;
	}
}
package breakout;

import java.awt.Image;
import java.awt.Rectangle;

import javax.swing.ImageIcon;

/**
 * A sprite is a visible object in a game. They have a position (x,y) in a
 * bidimensional space. They have an associated image, which has a rectangular
 * shape. Therefore, the image will have a width and height, and we will use the
 * rectangle shape of the image to detect collisions between objects by checking
 * the intersections between their corresponding rectangles.
 */
public abstract class Sprite {
	/** x coordinate of the object */
	protected int x;
	/** y coordinate of the object */
	protected int y;
	/** Width of the object */
	protected int width;
	/** Height of the object */
	protected int height;
	/** Graphical representation of the object */
	protected Image image;

	/**
	 * A sprite is created in the position passed as first two arguments, and using
	 * the image in the file passed as third argument to visualize it.
	 *
	 * @param nx is the initial value for the x coordinate
	 * @param ny is the initial value for the y coordinate
	 * @param f  is the file that contains the image to represent the visualization
	 *           of the sprite
	 */
	public Sprite(int nx, int ny, String f) {
		x = nx;
		y = ny;
		ImageIcon ii = new ImageIcon(this.getClass().getResource(f));
		image = ii.getImage();
		width = image.getWidth(null);
		height = image.getHeight(null);
	}

	/**
	 * Sets the x coordinate of the sprite object.
	 *
	 * @param nx new value for the x coordinate.
	 */
	public void setX(int nx) {
		x = nx;
	}

	/**
	 * Sets the y coordinate of the sprite object.
	 *
	 * @param ny new value for the y coordinate.
	 */
	public void setY(int ny) {
		// TO BE COMPLETED
	}

	/**
	 * Returns the x coordinate of the sprite object.
	 *
	 * @return int value of the x coordinate
	 */
	public int getX() {
		return x;
	}

	/**
	 * Returns the y coordinate of the sprite object.
	 *
	 * @return int value of the y coordinate
	 */
	public int getY() {
		// TO BE COMPLETED
	}

	/**
	 * Returns the width of the sprite object.
	 *
	 * @return int value of the width
	 */
	public int getWidth() {
		// TO BE COMPLETED
	}

	/**
	 * Returns the height of the sprite object.
	 *
	 * @return int value of the height
	 */
	public int getHeight() {
		// TO BE COMPLETED
	}

	/**
	 * Returns the image associated to the sprite object.
	 *
	 * @return Image object associated to the sprite
	 */
	Image getImage() {
		// TO BE COMPLETED
	}

	/*
	 * Independently of the image, we use the rectangle embedding the image to
	 * detect collisions. A rectangle is defined by the coordinates of its top-left
	 * corner and its width and height.
	 */
	public Rectangle getRectangle() {
		return new Rectangle(x, y, image.getWidth(null), image.getHeight(null));
	}

	@Override
	public String toString() {
		return String.format("%s(x=%s,y=%s,width=%s,height=%s)", this.getClass().getName(), getRectangle().x, getRectangle().y, getRectangle().width, getRectangle().height);
	}
}
